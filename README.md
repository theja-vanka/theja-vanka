<h1 align="center">Hi 👋, I'm Krishnatheja Vanka</h1>
<h3 align='center'>
    <samp>Data Scientist</samp>
</h3>

<br/>

<ul align='center' style="list-style-type:none;">
    <li>Building production ready <b>Machine Learning / Deep Learning</b> models at work.</li>
    <li>Levelling up on <b>Data Structures and Algorithm</b> by night.</li>
    <li>Technology Evangelist / Linux Power User</li>
    <li>Polyglot Programming</li>
</ul>
<p align='center'>
    <b>Connect</b> with me if you are into <b>Machine Learning / Deep Learning, Data Science, Coding</b>, or just to say <b>"Hi"</b> 👋.
</p>

<br/>

<div align='center'>
    <a href="mailto:theja.vanka@gmail.com" target="_blank" rel="noopener noreferrer">
        <img src="https://img.shields.io/badge/Mail_Me-c14438?style=for-the-badge&logo=Gmail&logoColor=white" alt="Mail-theja.vanka@gmail.com">
    </a>
    &nbsp;
    <a href="https://www.linkedin.com/in/krishnatheja-vanka/" target="_blank" rel="noopener noreferrer">
        <img src="https://img.shields.io/badge/LinkedIn-%230077B5.svg?&style=for-the-badge&logo=linkedin&logoColor=white" alt="LinkedIn/krishnatheja-vanka">
    </a>
    &nbsp;
    <a href="https://kaggle.com/thejavanka/" target="_blank" rel="noopener noreferrer">
        <img src="https://img.shields.io/badge/Kaggle-20BEFF.svg?&style=for-the-badge&logo=kaggle&logoColor=white" alt="Kaggle/thejavanka">
    </a>
</div>

<br/>

<div align='center'>
    <img src='https://github-readme-stats.vercel.app/api?username=theja-vanka&show_icons=true&count_private=true&include_all_commits=true&custom_title=My%20Github%20Stats&hide_border=true' alt='My-Github-stats'>
    <img src='https://github-readme-stats.vercel.app/api/top-langs/?username=theja-vanka&custom_title=Most%20Used%20Extensions&langs_count=6&hide_border=true&hide=html,css,MATLAB' alt='Top-Langs'>
</div>

<!--br/-->

<!--div align='center'>
    <img src='https://github-readme-stats.vercel.app/api/wakatime?username=thejavanka&layout=compact&hide_border=true' alt='My-Waka-stats'>
</div -->

<br/>

---

<div align='center' width="100%">
    <img src="https://devicons.github.io/devicon/devicon.git/icons/cplusplus/cplusplus-original.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://devicons.github.io/devicon/devicon.git/icons/python/python-original.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://devicons.github.io/devicon/devicon.git/icons/javascript/javascript-original.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://www.vectorlogo.zone/logos/apache_spark/apache_spark-icon.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://www.vectorlogo.zone/logos/tensorflow/tensorflow-icon.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://www.vectorlogo.zone/logos/pytorch/pytorch-icon.svg" width="25px">
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://devicons.github.io/devicon/devicon.git/icons/nodejs/nodejs-original.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://www.vectorlogo.zone/logos/pocoo_flask/pocoo_flask-icon.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://devicons.github.io/devicon/devicon.git/icons/ubuntu/ubuntu-plain.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://devicons.github.io/devicon/devicon.git/icons/vim/vim-plain.svg" width="25px">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <img src="https://devicons.github.io/devicon/devicon.git/icons/docker/docker-original.svg" width="25px">
</div>

<br/>

<div align='center'>
    <!--a href="https://theja-vanka.github.io/" target="_blank" rel="noopener noreferrer">
        <img src="https://img.shields.io/badge/website-4885ED?style=for-the-badge&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAAEAAAABACAQAAAAAYLlVAAAABGdBTUEAALGPC%2FxhBQAAACBjSFJNAAB6JgAAgIQAAPoAAACA6AAAdTAAAOpgAAA6mAAAF3CculE8AAAAAmJLR0QAAKqNIzIAAAAJcEhZcwAADsQAAA7EAZUrDhsAAAAHdElNRQfkCh8OIQn8FiwRAAAGw0lEQVRo3sVZW3CNVxT%2B%2FpNDR0TQBB2ZEkwGiSJR%2BlAeqKoYndalnSlJqNL2LdFO%2B4QaY0bdOogKrZrozXhqeaCjL0Va6pJx1xG9kEalcnJcIhxyfH2w%2Fn32v%2F%2F%2FnJzzR8d62ftfa%2B21vn%2FvtW9rW0iR2AtjMRJ5yEM2uiMDQAtu4BrqUIcTqLGaUrWYvOsiruFZPmAiesAzXM3CR%2B06gwv5G1Oh86xgxqNx3oPL2ZySc5tCXMbuHXMe4Dw2xjHfzE2qvikuxKucS8uv%2BwH8OY7ZKDcwg%2BnqO50ZrGQ0jnYNc%2F24f43XNSO3WavqrXwFAHQAAMBXeUdxjvO21jrMmak5t7haa36f6ziY9fJ1l%2BNFywAAcAIjwqnnYK7nfc3KyqSHgp34pdbwFxYA%2FEJ9lyo9FwCAZYq3FWABD2mWtrNTcu53qyZtXMQ0gM%2Bp8d2qaXoAALhNxckYgEEuYZvS290uBFra34c5Wbg%2FCOeyPrfjAOjGv4W7VzjFDGu9kHggHGPfyHEAwCLFecOh6wkA4GzFLwQAjuO%2Feiwkcv%2B6MYEirKDFnfJ10ok%2BLgCLp4S%2FkxYrVGDaFG9GcICaePp6v1%2BNYamhHweAFopt3K9Zsq2GPdcFBtSyE%2BVUVtKkejOAEgDorCZtjCo5VYVyjUckcJ5SXQQAnM%2B7DgP7mJM0gBzuc7S9w7cAgEsUZ67pvoda8w8xTXgFPOowc5%2B7WMqsRACYzTLu1iYeSR5hvkiD%2FFV4V41tistFcI9DNW5QWxNsauNxbubbnKTNjiJO4jvcwlrDNUnuYlCzWMB7wl%2Bmu%2B%2FKJmGvM3om3naUPB00LG4UfkhbU7hQmLdiHQwAzFB4q3g5JbeX%2BanUIsYk7cUWkVTEmPZpx%2Fz%2FF4V%2FkwEG%2BBI381K7ri%2BxipMYoKV6dYJhdYPwz9uMUcJ4wIGG6oci%2BUnjDWUJP%2BFentEOIc08wz1cyxIO0TR3ifR9w%2BogtSI8PDtyjduNqH4jkjXwoPjTUOQfifQrl%2BSASFYBAQDFwv%2FWZWO4lEfhh04bVmK0Q8rih0Fhd8jTLqR2uAz21QND7AhySXLVoGeD042QiKn1Ug4yfAHIVPIsl%2ByCSKYFVAcdc1noL%2BVtq8XPCFg30WpYipHtbXgAeVI94VLKlrLRj3tHy2yXxPaWFwPwh0upR4cBXJWyp0vyuw0giD5SvUlTra%2BUYboNAECXGFQ%2B4akRti25LNyQsrfFEJ70%2FYcdpyaLEXR%2BjAAigcfoHAAYgK8p9sioJYgWiYGJqDWEb2ItAGAPSjwbd0GD1HJwx1Pja0wBALyHakMyCj%2FaABrRDwCQaYWdOrwilZ6mROQRVb1utXpq2LF%2FxWXbPpA1BlAn1YGu9tel7AO%2F9JSU7h8YJGVdDMBIl5KdbvIPwG7pTlzZ3i4GcFKqo11Kl6Ts6i%2Fbw0ykG5ZiZHs7CWar7bify8T%2FtR0PEEmUWQGrCWeFX%2Byy8aeU%2FtJu%2BYaVGNmezlqhAAC5RmOWS%2FGUlM%2F6AvCMYSVG9i17LxAEsAMfAADGcaDl3BNPCCgtPpiPUShCPnIQu6o1sAENOIdaHLNiB5siZcVBHITnpWofzXhexmS9oTpRO5ansZifJXE7uMwtnMw07Vg%2B3rBqX3zPxVgVwrpFx9GBXdXFZLPHfTcR1assonkx6a3yZ%2BW6o2vC3GCg7fjV7IBh0QYmV7MgAFi3WYXFAIB3udlSXcMgQq4AiuIEjqAWfyGkdo8iZCEXRRiDkUgz9MMMWm3K4jAskGql46TJ7rwqyA4nuJ5%2Fx9lUxxfP63kWS%2Fi9Iz9IHmWB%2FUM8Irx%2FmGkA5RzVYDEAcIErQdHXaBE%2FQdHXSFDc5XwA4FLFKXP1LC0eFGGUU9UlWg%2BrjqZoXlYpmoOeyTrmqnyed5KqLGkAdm96J6ma2R%2FexBkG6gjLtTTdqaTTdKeFv5MWy11puhmIT1ypKdqJykLFmZUUgFL1x3aiUn9zWIFERIvblWqYsmlwr4qDbu0BYCYbhGunaqdoqdrqdnPmRrJ6CYMAx6jw2dYugGoVyqMBBrlUe8hoP1ktEGK9QB5iAcCt7gnkuQ7MUbzPAQ7jYc1SdVLuZSD0WLjPDdqDRYQvxAPAiY4Hi0rHgrQixbcjztRGzvlkc4fTvABwuvZkU%2Bt4smnm9JScC4Rc1tCbotzIbo5Hq0xuivukeSDuvE9iKOaqPcKkMKtUvcrxwKXTFZb5frYTEN25jCH6oSYudW05PkFksEKdmpKjsyxn10fiXINRyFU83c7jdZSn%2BDFHJG815fFhNsZihDzf90Q6gFaEcQ0XcQEnUWOFUrP3H03o95bINa6wAAAAJXRFWHRkYXRlOmNyZWF0ZQAyMDIwLTEwLTMxVDE0OjMzOjA5KzAwOjAwLvKEkAAAACV0RVh0ZGF0ZTptb2RpZnkAMjAyMC0xMC0zMVQxNDozMzowOSswMDowMF%2BvPCwAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAAElFTkSuQmCC&logoColor=white" alt="GithubPages/theja-vanka" />
    </a-->
    <img src='http://estruyf-github.azurewebsites.net/api/VisitorHit?user=theja-vanka&countColor=%230366d6' alt='Visits'>
    <!-- a href="https://sourcerer.io/theja-vanka" target="_blank" rel="noopener noreferrer">
        <img src="https://img.shields.io/badge/sourcerer-00E7AA?style=for-the-badge&logo=data%3Aimage%2Fpng%3Bbase64%2CiVBORw0KGgoAAAANSUhEUgAAACAAAAAoCAYAAACfKfiZAAAABmJLR0QA%2FwD%2FAP%2BgvaeTAAADPUlEQVRYhb3YXYxdUxQH8N9thwmDaqQ1bZTE9EOlia%2F46pN4omhIfbzwQBDhSfSFRBoviNRHoy8immYi2nqg8VFJEy%2B%2BpdFU0DLTMA0vaihCh6vFw9pnetzcc%2B65c8%2F1T1bu3Wevs9Z%2F7732Wnufhs44Dhfg%2FCQLcDLmpP4j%2BBHf4Wvsxi78UsF2KZZhPQ7iny6libdwC2Z36%2FgkPCVG1q3jdjKGq6o6X4GJmhy3ykYMlDm%2FVKxlP5xnsr2IxAJM9tl5fiam0Ui%2Fr%2BPasulJ2CuifAK%2FpWenYpHYKctzNstwDXZkjQs7MP4bo1hawfBiPIE%2FOtgck9sdG0oUj%2BK2Co5bsRzfdCBxdab8SYnS%2Bhk4z3AWfi6xPZopliWakR4IwP0ltsczpWaJ0lCPBOaLZSyKrcFZYpqKcHGPBA4maYcG5g5gH%2BYVKD2DyzHVA4lnRTy0w5%2FwkPJo%2FRKrMKsHEoVoiCQyUcHBAbwpEtG%2BROynOgjAVlE6u8UPich4TvbjK5GMKmNeeqmufN8Uh5KNWIPBKiSG8UGNJPIyKQK6KBinMSgOI2W5oRc5jLUqFKxFeERUv34Q2ayLXXUO7sZz%2BFh9h5Z1qkxFAU7DkpwsTr9LcUpFG02cO0P%2FpRjBXXhP51l4cqYzUBXX43EcX9C%2FP98YwZ04D9f1idAy3JR84L%2BR%2BKLIBfNxUR%2BcPy1S%2BMvYgy0YyM5l68TlYVwki734rEbna0R%2ByS%2F5ChzKZmA7fsWrIrevrtE5ufNfC1ZlBD4VNX9PIlA3igrTVF9qfBtsE0ewVmz5vwi8i3vwe2o38TC25oPiMlFCh3AmPq9geLb4TpA%2FmJyBE9roTonz4RJxZzhcZvgKvII7UnsE9%2Bb6B%2FAYvhAZb1caALyvfdb7sMKAprEhOdmZ2ttweq7%2FQVHfs218tgjk4W4JFN3Xj%2BLE9P9GvIPvc%2F0345KkR3yaeR43dBpZK4qC8CWRqUZxayK6Q6wfMfK%2FWt6ZxNxuCXTCo2Jt38ZCcdUi0umVOb0G3hB3iFqWgLjhzsFHYg8fcWzGHsBryekBsUy7i5yUoawcr8ULOIT7sFIc08ZS%2F5BI2cOO7QS4XWzjVnyLTa0P%2FwVZKcJjg4GZIQAAAABJRU5ErkJggg%3D%3D&logoColor=white"
        alt="Sourcerer/theja-vanka">
    </a-->    
</div>
